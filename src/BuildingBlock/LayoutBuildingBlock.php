<?php

namespace C33s\Toolkit\LayoutBundle\BuildingBlock;

use C33s\ConstructionKitBundle\BuildingBlock\SimpleBuildingBlock;

class LayoutBuildingBlock extends SimpleBuildingBlock
{
    /**
     * Return true if this block should be installed automatically as soon as it is registered (e.g. using composer).
     * This is the only public method that should not rely on a previously injected Kernel.
     *
     * @return bool
     */
    public function isAutoInstall()
    {
        return true;
    }

    /**
     * Get the fully namespaced classes of all bundles that should be enabled to use this BuildingBlock.
     * These will be used in AppKernel.php.
     *
     * @return array
     */
    public function getBundleClasses()
    {
        return array(
            'C33s\Toolkit\LayoutBundle\C33sToolkitLayoutBundle',
            'Sonata\SeoBundle\SonataSeoBundle',
            'SunCat\MobileDetectBundle\MobileDetectBundle',
        );
    }

    /**
     * Get list of parameters including their default values to add to parameters.yml and parameters.yml.dist if not set already.
     *
     * This will be called every time the building blocks are refreshed.
     *
     * @return array
     */
    public function getAddParameters()
    {
        return array(
            'node.bin' => '/usr/bin/node',
        );
    }
}

# c33s-toolkit/layout-bundle

Base layouts for Symfony 3 applications. Can optionally be integrated with
[c33s-toolkit/bootstrap3-bundle](https://packagist.org/packages/c33s-toolkit/bootstrap3-bundle),
[sonata-project/page-bundle](https://packagist.org/packages/sonata-project/page-bundle) and
[sonata-project/seo-bundle](https://packagist.org/packages/sonata-project/seo-bundle).

## Installation

Add the following to your `composer.json`:

```json
    "config": {
        "component-dir": "web/media/components",
        "component-baseurl": "media/components",
    },
```

Install composer package: `composer require c33s-toolkit/layout-bundle`.

Add bundles to your AppKernel.php:

```php
<?php

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new C33s\Toolkit\CoreConfigBundle\C33sToolkitCoreConfigBundle(),
            new C33s\Toolkit\LayoutBundle\C33sToolkitLayoutBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),
```

Copy the default config for `assetic` and `mobile_detect` from `src/Resources/config/defaults/`
for a quick start.
